package vcs;

import java.util.ArrayList;

import utils.ErrorCodeManager;
import utils.OperationType;

public class BranchOperation extends VcsOperation {
    public BranchOperation(OperationType type, ArrayList<String> operationArgs) {
        super(type, operationArgs);
    }

    @Override
    public int execute(Vcs vcs) {

        // Check if no branch, if so create one
        if (vcs.branches == null) {
            vcs.branches = new ArrayList<>();
            Branch newbranch = new Branch();
            newbranch.name = "master";
            vcs.branches.add(newbranch);
            vcs.HEAD = newbranch;
        }
        // Check if that branch exists
            for (Branch branch : vcs.branches) {
                if (branch.name.equals(operationArgs.get(0))) {
                    return ErrorCodeManager.VCS_BAD_CMD_CODE;
                }
            }
            // If we got here, we need to create the new branch
            // Clonate the filesys to update
            Commit newcommit = new Commit();
            newcommit.snapshot = vcs.getActiveSnapshot().cloneFileSystem();

            Branch newbranch = new Branch();
            newbranch.name = operationArgs.get(0);
            newbranch.commits = new ArrayList<>();
            newbranch.commits.add(newcommit);

            vcs.branches.add(newbranch);

        return ErrorCodeManager.OK;
    }
}
