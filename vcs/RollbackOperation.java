package vcs;

import java.util.ArrayList;

import utils.ErrorCodeManager;
import utils.OperationType;

public class RollbackOperation extends VcsOperation {
    public RollbackOperation(OperationType type, ArrayList<String> operationArgs) {
        super(type, operationArgs);
    }

    @Override
    public int execute(Vcs vcs) {
        // Clearing the stage and returning to the old filesys
        vcs.staging.clear();
        vcs.setActiveSnapshot(vcs.HEAD.commits.get(vcs.HEAD.commits.size() - 1).snapshot);
        return ErrorCodeManager.OK;
    }

}
