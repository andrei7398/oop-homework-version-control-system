package vcs;

import java.util.ArrayList;

import utils.ErrorCodeManager;
import utils.OperationType;

public class CheckoutOperation extends VcsOperation {
    public CheckoutOperation(OperationType type, ArrayList<String> operationArgs) {
        super(type, operationArgs);
    }

    @Override
    public int execute(Vcs vcs) {
        // If staging has not been used until now
        if (vcs.staging == null) {
            vcs.staging = new ArrayList<>();
        }
        // Check for commit/rollback error
        if (!vcs.staging.isEmpty()) {
            return ErrorCodeManager.VCS_STAGED_OP_CODE;
        }
        // Change on same branch, different commit
        if (operationArgs.get(0).equals("-c")) {
            operationArgs.remove(0);
            if (vcs.branches == null) {
                vcs.branches = new ArrayList<>();
            }
            int i = 0;
            for (Commit commit : vcs.HEAD.commits) {
                i++;
                // If we don't get inside the that commit doesn't exist
                if (commit.ID == Integer.parseInt(operationArgs.get(0))) {
                    vcs.HEAD.commits.subList(vcs.HEAD.commits.size() - i,
                            vcs.HEAD.commits.size()).clear();
                    vcs.setActiveSnapshot(commit.snapshot);
                    return ErrorCodeManager.OK;
                }
            }
            return ErrorCodeManager.VCS_BAD_PATH_CODE;
        } else {
            // Change on different branch
            for (Branch branch : vcs.branches) {
                if (branch.name.equals(operationArgs.get(0))) {
                    vcs.HEAD = branch;
                    return ErrorCodeManager.OK;
                }
            }
        }
        // Bad command error - no branch with that name
        return ErrorCodeManager.VCS_BAD_CMD_CODE;

    }
}
