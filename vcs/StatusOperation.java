package vcs;

import java.util.ArrayList;

import utils.ErrorCodeManager;
import utils.OperationType;

public class StatusOperation extends VcsOperation {
    public StatusOperation(OperationType type, ArrayList<String> operationArgs) {
        super(type, operationArgs);
    }

    @Override
    public int execute(Vcs vcs) {
        // Check if there is just one branch => master branch
        if (vcs.branches == null || vcs.branches.size() == 1) {
            vcs.getOutputWriter().write("On branch: master\n");
        } else {
            vcs.getOutputWriter().write("On branch: " + vcs.HEAD.name + "\n");
        }
        vcs.getOutputWriter().write("Staged changes:\n");
        // first is used to skip the space before first line
        boolean first = true;
        for (String op : vcs.staging) {
            if (!first) {
                vcs.getOutputWriter().write("\n");
            }
            vcs.getOutputWriter().write("\t" + op);
            first = false;
        }
        if (!vcs.staging.isEmpty()) {
            vcs.getOutputWriter().write("\n");
        }
        return ErrorCodeManager.OK;
    }

}
