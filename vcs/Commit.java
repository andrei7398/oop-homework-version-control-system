package vcs;

import filesystem.FileSystemSnapshot;

public class Commit {
    public String message;
    public int ID;
    public FileSystemSnapshot snapshot;

    public Commit() {
    }
}
