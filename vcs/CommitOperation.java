package vcs;

import java.util.ArrayList;

import utils.ErrorCodeManager;
import utils.IDGenerator;
import utils.OperationType;

public class CommitOperation extends VcsOperation {
    public CommitOperation(OperationType type, ArrayList<String> operationArgs) {
        super(type, operationArgs);
    }

    @Override
    public int execute(Vcs vcs) {
        // Check if staging is empty or hasn't been used until now
        if (vcs.staging == null || vcs.staging.isEmpty()) {
            return ErrorCodeManager.VCS_BAD_CMD_CODE;
        }
        // Create the commit, snap the filesys, set message and id then add
        Commit newcommit = new Commit();
        newcommit.snapshot = vcs.getActiveSnapshot().cloneFileSystem();
        operationArgs.remove(0);
        newcommit.message = String.join(" ", operationArgs);
        newcommit.ID = IDGenerator.generateCommitID();

        vcs.HEAD.commits.add(newcommit);
        // Clear the staging
        vcs.staging.clear();

        return ErrorCodeManager.OK;
    }
}
