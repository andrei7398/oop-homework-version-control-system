package vcs;

import java.util.ArrayList;

import filesystem.FileSystemOperation;
import filesystem.FileSystemSnapshot;
import utils.IDGenerator;
import utils.OutputWriter;
import utils.Visitor;

public final class Vcs implements Visitor {
    private final OutputWriter outputWriter;
    private FileSystemSnapshot activeSnapshot;
    public Branch HEAD;
    public ArrayList<String> staging;
    public ArrayList<Branch> branches;
    /**
     * Vcs constructor.
     *
     * @param outputWriter the output writer
     */
    public Vcs(OutputWriter outputWriter) {
        this.outputWriter = outputWriter;
    }

    /**
     * Does initialisations.
     */
    public void init() {

        this.activeSnapshot = new FileSystemSnapshot(outputWriter);
        this.staging = new ArrayList<>();
        this.branches = new ArrayList<>();

        Branch branch = new Branch();
        branch.commits = new ArrayList<>();
        branch.name = "master";

        Commit commit = new Commit();
        commit.ID = IDGenerator.generateCommitID();
        commit.message = "First commit";
        commit.snapshot = new FileSystemSnapshot(outputWriter);
        branch.commits.add(commit);
        this.branches.add(branch);
        this.HEAD = branch;
    }

    /**
     * Visits a file system operation.
     *
     * @param fileSystemOperation the file system operation
     * @return the return code
     */
    public int visit(FileSystemOperation fileSystemOperation) {
        return fileSystemOperation.execute(this.activeSnapshot);
    }

    /**
     * Visits a vcs operation.
     *
     * @param vcsOperation the vcs operation
     * @return return code
     */
    @Override
    public int visit(VcsOperation vcsOperation) {
        return vcsOperation.execute(this);
    }

    OutputWriter getOutputWriter() {
        return this.outputWriter;
    }
    FileSystemSnapshot getActiveSnapshot() {
        return this.activeSnapshot;
    }

    void setActiveSnapshot(FileSystemSnapshot cumva) {
        this.activeSnapshot = cumva;
    }
}
