package vcs;

import java.util.ArrayList;

import utils.ErrorCodeManager;
import utils.OperationType;

public class LogOperation extends VcsOperation {
    public LogOperation(OperationType type, ArrayList<String> operationArgs) {
        super(type, operationArgs);
    }

    @Override
    public int execute(Vcs vcs) {
        // first is used to skip space before the first line
        boolean first = true;
        for (Commit crtcommit : vcs.HEAD.commits) {
            if (!first) {
                vcs.getOutputWriter().write("\n\n");
            }
            vcs.getOutputWriter().write("Commit id: " + crtcommit.ID + "\n");
            vcs.getOutputWriter().write("Message: " + crtcommit.message);
            first = false;
        }
        vcs.getOutputWriter().write("\n");
        return ErrorCodeManager.OK;
    }
}
